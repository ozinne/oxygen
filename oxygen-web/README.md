# oxygen-web

轻量级web实现


## 介绍

一个轻量级web实现

oxygen-web

```
├─ src/main
  │─ java/.../web  //oxygen-web代码目录
  
```

## 特性

* 轻量级，注释完善，使用简单
* 使用ServiceLoader加载插件，易于扩展


## 安装

添加依赖到你的 pom.xml:
```
<dependency>
    <groupId>vip.justlive</groupId>
    <artifactId>oxygen-web</artifactId>
    <version>${oxygen.version}</version>
</dependency>

```

## 快速开始

